<?php

$Ident=(isset($_POST['Ident']))?$_POST['Ident']:"";
$Cant=(isset($_POST['Cant']))?$_POST['Cant']:"";
$accion=(isset($_POST['accion']))?$_POST['accion']:"";



include ("../conexion/conexion.php");

switch ($accion) {
    case 'agregar':
        $sentencia = $pdo->prepare("INSERT INTO clientes2 (ID,Nro_prod) VALUES (:ID,:Nro_prod) ");
        $sentencia->bindParam(':ID',$Ident);
        $sentencia->bindParam(':Nro_prod',$Cant);
        $sentencia->execute();
    break;
    
    case 'listar':
        $sentencia = $pdo->prepare("SELECT * FROM clientes2 WHERE 1");
        $lista_ventas=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->execute();
        header('Location: index.php');
    break;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" >
</head>
<body>
    <br>
    <div class="container">
        <h2>Agregar venta</h2>
        <form action="" method="post">
            <label for="">Número de identificación:</label>
            <input type="text" name="Ident" placeholder="" id="txt1" require=""><br>
            <br>
            <label for="">Cantidad de productos:</label>
            <input type="text" name="Cant" placeholder="" id="txt2" require=""><br>

            <button value="agregar" type="submit" name="accion">Agregar</button>
        </form>
        <br><br>
        <form action="" method="post">
            <h2>Consultar clientes</h2>
            <button value="listar" type="submit" name="accion">Consultar</button>
        
        </form>
    </div>
    <div class="row">
        <table>
            <thead>
                <tr>
                    <th>Identificación</th>
                    <th>Cantidad de productos</th>
                </tr>
            </thead>
            <?php foreach($lista_ventas as $clientes){ ?>
                <tr>
                    <td><?php echo $clientes['ID']; ?></td>
                    <td><?php echo $clientes['Nro_prod'];  ?></td>
                </tr>
            <?php } ?>
        </table>
    
    </div>
</body>
</html>